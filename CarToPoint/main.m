//
//  main.m
//  CarToPoint
//
//  Created by Krazyzy on 13.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
