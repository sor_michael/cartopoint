//
//  ViewController.h
//  CarToPoint
//
//  Created by Krazyzy on 13.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *carIv;

@end

