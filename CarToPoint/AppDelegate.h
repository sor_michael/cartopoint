//
//  AppDelegate.h
//  CarToPoint
//
//  Created by Krazyzy on 13.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

