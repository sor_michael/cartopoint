//
//  ViewController.m
//  CarToPoint
//
//  Created by Krazyzy on 13.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)didTapOnScreen:(UITapGestureRecognizer*)sender {
    CGPoint location = [sender locationInView:self.view];
    
    CGFloat angle = M_PI + atan2(self.carIv.center.y - location.y,
                                 self.carIv.center.x - location.x) + M_PI * 0.5;
    
    [UIView animateWithDuration: .25
                     animations: ^{
                         self.carIv.transform = CGAffineTransformMakeRotation(angle);
                     } completion:^(BOOL finished){
                         [UIView animateWithDuration:1.0
                                          animations:^{
                                              self.carIv.center = location;
                                          }];
                     }];

}



@end
